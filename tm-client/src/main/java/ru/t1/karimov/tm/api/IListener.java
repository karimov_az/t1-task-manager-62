package ru.t1.karimov.tm.api;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.karimov.tm.event.ConsoleEvent;

public interface IListener {

    @NotNull
    String getName();

    @Nullable
    String getArgument();

    @NotNull
    String getDescription();

    void handler(ConsoleEvent consoleEvent) throws Exception;

}
