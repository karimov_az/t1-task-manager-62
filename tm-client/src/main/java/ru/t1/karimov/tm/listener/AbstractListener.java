package ru.t1.karimov.tm.listener;

import lombok.Getter;
import lombok.Setter;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import ru.t1.karimov.tm.api.IListener;
import ru.t1.karimov.tm.api.service.IServiceLocator;
import ru.t1.karimov.tm.enumerated.Role;
import ru.t1.karimov.tm.event.ConsoleEvent;

@Getter
@Setter
@Component
public abstract class AbstractListener implements IListener {

    @NotNull
    @Autowired
    protected IServiceLocator serviceLocator;

    @NotNull
    public abstract String getName();

    @Nullable
    public abstract String getArgument();

    @NotNull
    public abstract String getDescription();

    @Nullable
    public abstract Role[] getRoles();

    public abstract void handler(@NotNull final ConsoleEvent consoleEvent) throws Exception;

    @Nullable
    protected String getToken() {
        return getServiceLocator().getTokenService().getToken();
    }

    protected void setToken(@Nullable final String token) {
        getServiceLocator().getTokenService().setToken(token);
    }

    @NotNull
    @Override
    public String toString() {
        @NotNull final String name = getName();
        @Nullable final String argument = getArgument();
        @NotNull final String description = getDescription();
        @NotNull String result = "";
        final String colonComma = (argument == null || argument.isEmpty()) ? " : " : " , ";
        if (!name.isEmpty()) result += name + colonComma;
        if (argument != null && !argument.isEmpty()) result += argument + " : ";
        if (!description.isEmpty()) result += description;
        return result;
    }

}
