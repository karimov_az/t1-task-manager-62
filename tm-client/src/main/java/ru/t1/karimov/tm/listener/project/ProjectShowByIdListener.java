package ru.t1.karimov.tm.listener.project;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.context.event.EventListener;
import org.springframework.stereotype.Component;
import ru.t1.karimov.tm.dto.model.ProjectDto;
import ru.t1.karimov.tm.dto.request.project.ProjectGetByIdRequest;
import ru.t1.karimov.tm.dto.response.project.ProjectGetByIdResponse;
import ru.t1.karimov.tm.event.ConsoleEvent;
import ru.t1.karimov.tm.util.TerminalUtil;

@Component
public final class ProjectShowByIdListener extends AbstractProjectListener {

    @NotNull
    public static final String DESCRIPTION = "Show project by id.";

    @NotNull
    public static final String NAME = "project-show-by-id";

    @NotNull
    @Override
    public String getDescription() {
        return DESCRIPTION;
    }

    @NotNull
    @Override
    public String getName() {
        return NAME;
    }

    @Override
    @EventListener(condition = "@projectShowByIdListener.getName() == #consoleEvent.name")
    public void handler(@NotNull final ConsoleEvent consoleEvent) throws Exception {
        System.out.println("[PROJECT LIST BY ID]");
        System.out.println("ENTER ID:");
        @Nullable final String id = TerminalUtil.nextLine();
        @NotNull final ProjectGetByIdRequest request = new ProjectGetByIdRequest(getToken());
        request.setId(id);
        @NotNull final ProjectGetByIdResponse response = projectEndpoint.getProjectById(request);
        @Nullable final ProjectDto project = response.getProject();
        showProject(project);
    }

}
