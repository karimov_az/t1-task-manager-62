package ru.t1.karimov.tm.service.dto;

import lombok.NoArgsConstructor;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import ru.t1.karimov.tm.api.service.dto.IProjectDtoService;
import ru.t1.karimov.tm.dto.model.ProjectDto;
import ru.t1.karimov.tm.enumerated.Status;
import ru.t1.karimov.tm.exception.entity.ProjectNotFoundException;
import ru.t1.karimov.tm.exception.field.*;
import ru.t1.karimov.tm.repository.dto.IProjectDtoRepository;

import java.util.Optional;

@Service
@NoArgsConstructor
public final class ProjectDtoService extends AbstractUserOwnedDtoService<ProjectDto> implements IProjectDtoService {

    @NotNull
    @Autowired
    private IProjectDtoRepository repository;

    @NotNull
    @Override
    @Transactional
    public ProjectDto changeProjectStatusById(
            @Nullable final String userId,
            @Nullable final String id,
            @Nullable final Status status
    ) {
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        if (id == null || id.isEmpty()) throw new IdEmptyException();
        if (status == null) throw new StatusEmptyException();
        @NotNull final ProjectDto project = Optional
                .ofNullable(repository.findByUserIdAndId(userId, id))
                .orElseThrow(ProjectNotFoundException::new);
        project.setStatus(status);
        repository.save(project);
        return project;
    }

    @NotNull
    @Override
    @Transactional
    public ProjectDto changeProjectStatusByIndex(
            @Nullable final String userId,
            @Nullable final Integer index,
            @Nullable final Status status
    ) {
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        if (index == null || index < 0) throw new IndexIncorrectException();
        if (index >= getSize(userId)) throw new IndexIncorrectException(ERROR_INDEX_OUT_OF_BOUNDS);
        if (status == null) throw new StatusEmptyException();
        @NotNull final ProjectDto project = Optional
                .ofNullable(findOneByIndex(userId, index))
                .orElseThrow(ProjectNotFoundException::new);
        project.setStatus(status);
        repository.save(project);
        return project;
    }

    @NotNull
    @Override
    @Transactional
    public ProjectDto create(@Nullable final String userId, @Nullable final String name) {
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        if (name == null || name.isEmpty()) throw new NameEmptyException();
        @NotNull final ProjectDto project = new ProjectDto();
        project.setName(name);
        @NotNull final ProjectDto result = add(userId, project);
        return result;
    }

    @NotNull
    @Override
    @Transactional
    public ProjectDto create(
            @Nullable final String userId,
            @Nullable final String name,
            @Nullable final String description
    ) {
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        if (name == null || name.isEmpty()) throw new NameEmptyException();
        if (description == null || description.isEmpty()) return create(userId, name);
        @NotNull final ProjectDto project = new ProjectDto();
        project.setName(name);
        project.setDescription(description);
        @NotNull final ProjectDto result = add(userId, project);
        return result;
    }

    @NotNull
    @Override
    @Transactional
    public ProjectDto create(
            @Nullable final String userId,
            @Nullable final String name,
            @Nullable final Status status
    ) {
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        if (name == null || name.isEmpty()) throw new NameEmptyException();
        if (status == null) return create(userId, name);
        @NotNull final ProjectDto project = new ProjectDto();
        project.setName(name);
        project.setStatus(status);
        @NotNull final ProjectDto result = add(userId, project);
        return result;
    }

    @NotNull
    @Override
    @Transactional
    public ProjectDto updateProjectById(
            @Nullable final String userId,
            @Nullable final String id,
            @Nullable final String name,
            @Nullable final String description
    ) {
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        if (id == null || id.isEmpty()) throw new IdEmptyException();
        if (name == null || name.isEmpty()) throw new NameEmptyException();
        if (description == null) throw new DescriptionEmptyException();
        @NotNull final ProjectDto project = Optional
                .ofNullable(findOneById(userId, id))
                .orElseThrow(ProjectNotFoundException::new);
        project.setName(name);
        project.setDescription(description);
        repository.save(project);
        return project;
    }

    @NotNull
    @Override
    public ProjectDto updateProjectByIndex(
            @Nullable final String userId,
            @Nullable final Integer index,
            @Nullable final String name,
            @Nullable final String description
    ) {
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        if (index == null || index < 0) throw new IndexIncorrectException();
        if (index >= getSize(userId)) throw new IndexIncorrectException(ERROR_INDEX_OUT_OF_BOUNDS);
        if (name == null || name.isEmpty()) throw new NameEmptyException();
        if (description == null) throw new DescriptionEmptyException();
        @NotNull final ProjectDto project = Optional
                .ofNullable(findOneByIndex(userId, index))
                .orElseThrow(ProjectNotFoundException::new);
        project.setName(name);
        project.setDescription(description);
        repository.save(project);
        return project;
    }

}
